# Laravel Api Foundation
LAF Package includes all helpers/classes/traits to build the restfull api in the professional way.
  - Localization middleware to determine and auto set the locale
  - Conflict system

# Features
## Localization middleware
Determine and set the locale from request Accept-Language header.

### Setup
Add code bellow into __app/Http/Kernel.php__
```php
    'api' => [
        \NavinLab\LaravelApiFoundation\Middleware\Localization::class,
        //...
    ],
```
## Conflict system
Allows to generate the conflict data response (409 http response code number).

### Usage
Add this code into the policy
```php
    use NavinLab/LaravelApiFoundation/Http/Exceptions/ConflictException;
    class SomePolicy 
    {
        //...
        public function action(User $user, Ability $ability) {
            // some check
            if (true) {
                throw new ConflictException('conflict-type', compact('user', 'ability'));
            }
        }
    }
```

### Instalation
```sh
    composer require navinlab/laravel-api-foundation
```

### Development
Want to contribute? Great!
- Clone pacakge to the folder
- Add composer.json repository section of the project bellow
```json
    "repositories": [
        {
            "type": "path",
            "url": "<path>/laravel-api-foundation",
            "options": {
                "symlink": true
            }
        }
    ],
```
- Set version attribute inside composer.json of the package (more then current)
- Run composer update

### Authors
  * Anton Pavlov <anton.pavlov.it@gmail.com>
  