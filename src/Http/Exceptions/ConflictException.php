<?php
/**
 * ConflictException
 * Helps to generate conflict response
 *
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */
namespace NavinLab\LaravelApiFoundation\Http\Exceptions;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class ConflictException extends HttpResponseException
{
    /**
     * ConflictException constructor.
     *
     * @param string $type  Unique conflict type name
     * @param array $data   Additional resolve conflict data
     */
    function __construct($type, array $data = [])
    {
        parent::__construct(new JsonResponse(array_merge($data, compact('type')), 409));
    }
}