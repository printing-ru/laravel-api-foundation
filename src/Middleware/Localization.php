<?php

namespace NavinLab\LaravelApiFoundation\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\App;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->getPreferredLanguage();
        if (!is_null($locale)) {
            App::setLocale($locale);
            Carbon::setLocale($locale);
        }
        return $next($request);
    }
}
