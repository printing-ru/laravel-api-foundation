<?php
namespace Tests;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use NavinLab\LaravelApiFoundation\Http\Exceptions\ConflictException;
use PHPUnit\Framework\TestCase;
class ConflictExceptionTest extends TestCase
{

    /**
     * Handle method test
     *
     */
    public function testConstructor()
    {
        $data = ['value' => rand(0, 1000)];
        $type = 'unique_type';

        $exception = new ConflictException($type, $data);

        //chould be instance of HttpResponseException
        $this->assertInstanceOf(HttpResponseException::class, $exception);

        $response = $exception->getResponse();
        //response should be instance of JsonResponse
        $this->assertInstanceOf(JsonResponse::class, $response);

        //check result in the response
        $responseData = $response->getData(true);
        $this->assertArrayHasKey('type', $responseData);
        $this->assertArraySubset(array_merge($data, compact('type')), $responseData);

        //check response status code
        $this->assertEquals(409, $response->getStatusCode());

    }
}