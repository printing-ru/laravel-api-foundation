<?php
namespace Tests;

use \Mockery as m;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use NavinLab\LaravelApiFoundation\Middleware\Localization;
use PHPUnit\Framework\TestCase;
class LocalizationTest extends TestCase
{
    protected $app;
    protected $request;
    protected $middleware;
    protected $next;
    protected $carbon;

    /**
     * @inheritdoc
     */
    protected function setUp() {
        parent::setUp();
        $this->app = m::mock(sprintf('alias:%s', App::class));
        $this->carbon = m::mock(sprintf('alias:%s', Carbon::class));
        $this->request = m::mock(Request::class);
        $this->middleware= new Localization();
        $this->next = function () {return 'next';};
    }

    /**
     * Handle method test
     *
     */
    public function testHandle()
    {
        $this->app->shouldReceive('setLocale')->once()->with('en');
        $this->carbon->shouldReceive('setLocale')->once()->with('en');
        $this->request->shouldReceive('getPreferredLanguage')->times(2)->andReturn('en', null);

        $this->middleware->handle($this->request, $this->next);
        $response = $this->middleware->handle($this->request, $this->next);

        $this->assertEquals('next', $response);
    }

    /**
     * @inheritdoc
     */
    public function tearDown()
    {
        m::close();
    }
}